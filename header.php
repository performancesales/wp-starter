<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
	<header class="main-header" id="main-header">
		<div class="container-fluid">
			<div class="menu-wrapper" id="js-main-menu">
				<?php 
					wp_nav_menu( 
						array( 
							'theme_location'  => 'primary', 
							'container'       => 'nav', 
							'container_class' => 'main-menu', 
						) 
					); 
				?>
			</div>
		
			<button class="menu-toggle" id="js-toggle-menu">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>
	</header>
	<div id="content" class="site-content">



