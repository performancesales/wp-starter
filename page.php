<?php get_header(); ?>

<div class="page">
	<div class="container-fluid">
		<?php while ( have_posts() ) : the_post(); ?>
		
			<?php the_content(); ?>	
		
		<?php endwhile; // end of the loop. ?>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
