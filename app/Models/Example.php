<?php 

namespace app\Models;

class Example {

    public static function get_all_exemple_posts(){

        $args = array(
            'post_type'      => 'example',
            'posts_per_page' => -1,
            'post_status'    => 'publish',
        );

        $query = new \WP_Query($args);

        if( $query->have_posts() ){

            $posts = array();

            while( $query->have_posts() ){
                $query->the_post();
               
                $id = get_the_ID();
                $posts[] = array(
                    'id'      => $id,
                    'title'   => get_the_title( ),
                    'link'    => get_the_permalink(),
                    'content' => apply_filters( 'the_content', get_the_content() )
                );
            }
            wp_reset_postdata();

            return $posts;

        }

        return false;
    }




}


?>