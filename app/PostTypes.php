<?php

namespace app;

/**
 * Rgister Custom Post Types
 *
 * @package app
 */
class PostTypes {
	
	
	/**
	 * Constructor
	 */
	public function __construct() {
		//add_action( 'init', array( $this, 'register_cpt_example' ) );
	}


	public function register_cpt_example(){
		$labels = array(
			"name" => __( "Example", "textdomain" ),
			"singular_name" => __( "Example", "textdomain" ),
			"add_new" => __( "Add Example", "textdomain" ),
			"add_new_item" => __( "Add New Example", "textdomain" ),
			"edit_item" => __( "Edit Example", "textdomain" ),
			"new_item" => __( "New Example", "textdomain" ),
		);

		$args = array(
			"label" => __( "Example", "textdomain" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"delete_with_user" => false,
			"show_in_rest" => true, 
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"exclude_from_search" => true,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"query_var" => true,
			"rewrite" => array( "slug" => "exemple", "with_front" => true  ),
			"menu_icon" => "dashicons-admin-post",
			"supports" => array( "title", "editor", "thumbnail" ),
		);

		register_post_type( "example", $args );
	}


}

