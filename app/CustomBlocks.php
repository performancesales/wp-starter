<?php 

namespace app;

class CustomBlocks {

	public function __construct(){

		if( function_exists('acf_register_block_type') ) {

			// Register custom category
			//add_filter( 'block_categories', array( $this, 'custom_block_categories' ), 10, 2 );

			// Custom blocks
			//$this->example_custom_block();
		}

	}

	public function example_custom_block(){
		acf_register_block_type(array(
			'name'              => 'example',
			'title'             => __('Example', 'textdomain'),
			'render_template'   => 'guttemberg/example.php',
			'mode'              => 'auto',
			'enqueue_style'     => get_template_directory_uri() . '/style.css',
			'category'          => 'custom-blocks',
			'icon'              => 'admin-customizer',
			'keywords'          => array( '' ),
			'post_types'        => array( 'page' ),
		));
	}


	public function custom_block_categories( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'custom-blocks',
					'title' => __( 'Custom', 'contacecao' ),
				),
			)
		);
	}


}


?>