app.filters = {

	filters: {
		'techniques':[],
		'years':[],
		'artists':[]
	},
	
	currentPostYpe:null,
	currentCategory:null,

	init:function(){
		this.subMenuActions();
		this.filterSelection();
	},




	subMenuActions:function(){

		var submenuNav = $('.top-menu');
		var submenus = $('.open-menu');
		var self = this;

		if( submenuNav.length > 0 && submenus.length > 0 ){

			var li = submenuNav.find('li');
			self.currentPostYpe = $('.nav-submenu').data('cpt');
			self.currentCategory = $('.nav-submenu').data('cat');
			
			submenuNav.find('a').click(function(e){
				e.preventDefault();

				var item = $(this);
				var parent = item.parent('li');
				var target = $("#"+item.attr('href'));

				if( parent.hasClass('current') ){
					parent.removeClass('current');
					submenus.addClass('hidden');
					return;
				}

				li.removeClass('current');
				parent.addClass('current');

				submenus.addClass('hidden')
				target.removeClass('hidden');

			});

		}

	},



	filterSelection:function(){

		var filterMenus = $('.open-menu');
		var self = this;

		if( filterMenus.length > 0 ){

			var filters = filterMenus.find('a');

			filters.click(function(e){
				e.preventDefault();

				var filter = $(this);
				var parent = filter.parent('li');
				var subMenu = parent.parent('ul');
				var selectedfilter = subMenu.data('filter');
				var filterValue = filter.data('filter-val');
				var currentFilter = self.filters[selectedfilter];
				var topMenuFilter = $('#' + selectedfilter + "-filters").parent('li');

				parent.toggleClass('current');

				if( parent.hasClass('current') ){
					currentFilter.push(filterValue);
					topMenuFilter.addClass('has--active--filters');
				} else {
					var index = currentFilter.indexOf(filterValue);
					if (index > -1) {
						currentFilter.splice(index, 1);
						topMenuFilter.removeClass('has--active--filters');
					}
				}

				self.filterSort();
				self.showClearAll();
			});

		}

	},


	showClearAll:function(){
		var clear = $('#clear-filter');
		var self = this;
		var filters = self.filters;
		if( filters.techniques == 0 && filters.years == 0 && filters.artists == 0 ){
			clear.addClass('hidden');
		} else {
			clear.removeClass('hidden');
			clear.click(function(e){
				e.preventDefault();
				self.filters= {
					'techniques':[],
					'years':[],
					'artists':[]
				}
				$('.top-menu').find('li').removeClass('current').removeClass('has--active--filters');
				$('.open-menu').addClass('hidden');
				$('.open-menu').find('li').removeClass('current');
				self.filterSort();
			});
		}

	},


	filterSort:function(){
		var self = this;

		var initialContent = $('#initial-content');
		var ajaxContent = $('#ajax-content');
		var filters = self.filters;
		var spinner = $('#spinner');

		if( filters.techniques == 0 && filters.years == 0 && filters.artists == 0 ){
			initialContent.removeClass('hidden');
			ajaxContent.html("").addClass('hidden');
			$('.load-more-wrapper').removeClass('hidden');
			self.showClearAll();
		} else {

			spinner.removeClass('hidden');

			var ajaxData = {
				action: 'ajax_'+ self.currentPostYpe +'_filter_handler',
				category:self.currentCategory,
				nonce: ajaxObj.nonce,
				techniques: filters.techniques,
				years: filters.years,
				artists: filters.artists
			};

			//console.log(ajaxData);

			jQuery.ajax({
				url: ajaxObj.ajaxurl,
				type: 'post',
				data: ajaxData,
				success:function(data){
					spinner.addClass('hidden');
					initialContent.addClass('hidden');
					ajaxContent.html(data).removeClass('hidden');
					$('.load-more-wrapper').addClass('hidden');
					console.log(data);
				},
				error: function(xhr, textStatus, errorThrown){
					//console.log(xhr);
					//console.log(textStatus);
					//console.log(errorThrown);
				}
			});


		}


	}


}