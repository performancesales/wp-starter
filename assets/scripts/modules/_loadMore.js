app.loadMore = {

	init:function(){
		if( $('.gsalgarias-news').length > 0 ){
			this.ajaxLoadMore();
		}
		if( $('.load-more-related').length > 0 ){
			this.jsLoadMore();
		}

	},


	jsLoadMore:function(){

		var button = $('#js-load-more-related');
		var container = $('.load-more-related');
		var rows = container.find('.row');
		var offset = 2;
		button.click(function(e){
			e.preventDefault();

			offset += 2;
			$.each(rows, function(index, el) {
				var curr = index + 1;
				var $el = $(el);
				if( curr <= offset ){
					$el.removeClass('hidden');
				}
			});
		
			if( container.find('.row.hidden').length == 0 ){
				button.addClass('disabled');
			}

		});

	},


	ajaxLoadMore:function(){

		var button = $('#js-load-more');
		var total = parseInt( button.data('total') );
		var container = $('.entries-wrapper');
		var offset = 12;
		var canBeLoaded = true;
		button.click(function(e){
			e.preventDefault();

			$.ajax({
				url: ajaxObj.ajaxurl,
				type: 'post',
				data: {
					action: 'load_more_handler',
					offset: offset,
				},
				beforeSend: function( xhr ){
					canBeLoaded = false; 
				},
				success:function(data, status){
					
					if( status == 'success' ){
						canBeLoaded = true;
						offset = offset + 12;
						container.append(data);
					
						if( $('.card-wrapper').length === total ){
							button.addClass('hidden');
						}


					}

				}
			})


		});
		
	}

}