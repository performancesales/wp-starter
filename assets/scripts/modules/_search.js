app.search = {

	init:function(){

		var search = $('#js-search');
		var open = $('#open-search');
		var close = $('#close-search');

		open.click(function(e) {
			e.preventDefault();

			if( !search.hasClass('search--active') ){
				search.addClass('search--active');
			}

		});

		close.click(function(e) {
			e.preventDefault();

			if( search.hasClass('search--active') ){
				search.removeClass('search--active');
			}

		});


	}

}