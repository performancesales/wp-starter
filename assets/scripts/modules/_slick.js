app.slick = {

	logo: null,

	init: function(){

		this.logo = $('#main-header').find('.logo-wrapper img').attr('src');

		this.workLightbox();
		this.galleryLightbox();
	},

	

	workLightbox: function(){
		var self = this;
		var lightbox = $('.js-lightbox');
		var cards = $('.art-card');
		if( lightbox.length > 0 && cards.length > 0 ){
			$('.js-lightbox').slickLightbox({
				itemSelector        : '.lightbox-img',
				navigateByKeyboard  : true,
				caption: 'caption',
				background: 'rgba(255,255,255,1)',
				layouts: { 
					closeButton: '<button type="button" class="slick-lightbox-close"></button>',
					siteLogo:'<img class="slick-lighbox-sitelogo" src="'+self.logo+'" alt="gsalgueiras">' 
				},
			});
			$('body').on('click', '.work-reservation-anchor', function(e) {
				var href = $(this).attr('href');
				window.location.href = location.protocol + "//" + location.host + href;
			});
			$('body').on('click', '.share-fb', function(e) {
				var href = $(this).attr('href');
				var win = window.open(href, '_blank');
				win.focus()
			});
			$('body').on('click', '.share-tw', function(e) {
				var href = $(this).attr('href');
				var win = window.open(href, '_blank');
				win.focus()
			});
			$('body').on('click', '.share-email', function(e) {
				var href = $(this).attr('href');
				window.location.href = $(this).data('mailto');
			});
		}

	},


	galleryLightbox:function(){
		var self = this;
		var gallery = $('.wp-block-gallery');
		if( gallery ){
			$( gallery ).slickLightbox({
				itemSelector        : 'a',
				navigateByKeyboard  : true,
				background: 'rgba(255,255,255,1)',
				caption:  function(element, info) { 
					var $element = $(element);

					if( $element.next().length > 0 ){
						var sibling = $element.next()
						return sibling.text();
					} 
					return "";
				},
				layouts: { 
					closeButton: '<button type="button" class="slick-lightbox-close"></button>',
					siteLogo:'<img class="slick-lighbox-sitelogo" src="'+self.logo+'" alt="gsalgueiras">' 
				},
			});
		}


	}



}