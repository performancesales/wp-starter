app.cookies = {

	init:function(){
		if( !localStorage.getItem("setCookie") ){
			this.cookieNotification();
		}
	},

	cookieNotification:function(){
		var cookieBar = $('.js-cookie-notification');
		cookieBar.css({ 'display' : 'block'});
		cookieBar.find('.cookie-button').click(function(event) {
			// Set session storage
			event.preventDefault();
			localStorage.setItem("setCookie", "set");
			cookieBar.fadeOut( "slow" );
		});
	}


}