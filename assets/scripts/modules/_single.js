app.single = {

	init:function(){
		this.printAction();
		this.readMore();
	},

	printAction:function(){
		$('#js-print').click(function(e){
			e.preventDefault();
			window.print();
		});
	},

	readMore:function(){
		var content = $('.entry-content.read-more');
		var moreContent = $('.read-more-content');
		if( content.length > 0 && moreContent.length ){

			var last = content.children().last().prev();
			last.append('<a href="#" class="read-more-link" id="js-read-more">info</a>');

			$('#js-read-more').click(function(e){
				e.preventDefault();
				moreContent.toggleClass('visible');
				$(this).toggleClass('active');
			});

		}
	}


}