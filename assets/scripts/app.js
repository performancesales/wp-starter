"use strict";

// Inicia namespace
var app = {};

$ = jQuery;

// Plugins (sem ser minified)
/*//=require plugins/*.js*/

// Módulos
//=require modules/*.js

// Funções que correm quando em dom ready
//=require _init.js